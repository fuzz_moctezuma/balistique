#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 27 10:56:59 2021

@author: cactus
"""

## DOCUMENTING MISSING DATES in BAL datasets 2021 05 27
list_json_datasets = []
for json_file in os.listdir('./data/json'):
    if 'bal_datasets_' in json_file:
        list_json_datasets.append(json_file)
list_json_datasets.sort(reverse=False)

# df = parse_bal_datasets(read_json_bal_datasets(load_json_from_file(name=list_json_datasets[0])))
m = pd.DataFrame(index=list_json_datasets ,columns=['dates_manquantes','deja_manquantes'])

missing = []
for i in list_json_datasets:
    df = parse_bal_datasets(read_json_bal_datasets(load_json_from_file(name=i)))
    m.loc[i,'dates_manquantes'] = df['commune_dateMAJ'].isna().value_counts().values[1]  
    for r, row in df[ df['commune_dateMAJ'].isna() ].iterrows():
        if row.commune_code not in missing:
            missing.append(row.commune_code)
    print(len(missing))
    m.loc[i,'deja_manquantes']=len(missing)
m['dates_manquantes'].to_csv('missing_date_in_jsons.csv')

#-------------------
#BOKEH ZOMBIESS
#--------------------------------------------------------------------------------------------------------------------

    lancement_bal = datetime(2020, 9, 1)
    launch_bal = Span(location=lancement_bal,
                          dimension='height', line_color='red',
                          line_dash='dashed', line_width=1, line_alpha=0.5,
                          name='Lancement de la Startup BAL')    
    p.add_layout(launch_bal)
#     start_date_adr = time.mktime(datetime(2020, 10, 8, 0, 0, 0).timetuple())*1000
#     launch_adr = Span(location=start_date_adr,
#                       dimension='height', line_color='#47b881',
#                       line_dash='dashed', line_width=1, line_alpha=0.5,
#                       name="Lancement de l'outil Mes Adresses")

#     p.add_layout(launch_adr)


#--------------------------------------------------------------------------------------------------------------------

#     p.legend.location = "top_left"
#     p.legend.click_policy="hide"
#     p.legend.location = 'top_left'
#     f.legend.label_text_color = 'olive'
#     f.legend.label_text_font = 'times'
#     f.legend.border_line_color = 'black'
#     f.legend.margin = 10
#     f.legend.padding = 18












