#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 27 23:52:11 2021

@author: cactus
"""
# ---------------
#     IMPORTS
# ---------------
import os
from datetime import datetime
import json
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import logging
from bs4 import BeautifulSoup



# ---------------------------------------------------------------------
# FONCTIONS : Récupération de JSON sur les APIS adresse.data et geo.api
# ----------------------------------------------------------------------
def request_json_data(url):
    """
    Return JSON object from an API endpoint.
    """
    # Uses Requests to download from URL
    r = requests.get(url)
    # if data is received, return a JSON object
    if r.status_code == requests.codes.ok:
        print('Request completed.')
        return r.json()
    else:
        print('ERROR : '+str(r.status_code))

def dump_json(js,json_name):
    """
    Write a JSON to a file in folder /data/json/ with the date
    """
    # Get todays date
    dt_string = datetime.now().strftime("%Y_%m_%d_%H%M")
    # Create folder if none existant
    path = './data/json/'
    if not os.path.exists(path):
        os.makedirs(path)
    #Write the json to file 
    with open(path+json_name+'_'+dt_string+'.json', "w") as outfile:
        json.dump(js, outfile)
    
def query_mes_adresses():
    """
    Write JSON file to /data/json/ with all BAL datasets metadatas published with "Mes Adresses".
    """
    logging.basicConfig(level=logging.DEBUG,filename='./logs/lumberjack.log')
    # Get data from the API
    url = 'https://backend.adresse.data.gouv.fr/publication/submissions/published'
    r = request_json_data(url)
    # Write JSON to a file
    dump_json(r,'mes_adresses')
    
    
def query_bal():
    """
    Write JSON file to /data/json/ with all BAL datasets metadatas.
    """
    logging.basicConfig(level=logging.DEBUG,filename='./logs/lumberjack.log')
    # Get data from the API
    url = 'https://backend.adresse.data.gouv.fr/datasets'
    r = request_json_data(url)
    # Write JSON to a file
    dump_json(r,'bal')
    
    
# ----------------
# Query ADM
# ----------------
def query_decoupage_administratif():
    """
    Write JSON file to /data/json/ with all Communes metadatas
    """
    logging.basicConfig(level=logging.DEBUG)
    # Get data from the API
    url_decoupage_administratif = 'https://geo.api.gouv.fr/communes'
    r = request_json_data(url_decoupage_administratif)
    # Write JSON to a file
    dump_json(r,'decoupage_administratif')

# ----------------
# Query BAN
# ----------------
def query_ban():
    """
    Write csv.gz files to /data/gz/ with all BAN adresses and lieux-dits
    """
    logging.basicConfig(level=logging.DEBUG)
    
    url = "https://adresse.data.gouv.fr/data/ban/adresses/latest/csv/"

    s = requests.Session()
    retries = Retry(total=10, backoff_factor=1, status_forcelist=[ 502, 503, 504 ])
    s.mount('https://', HTTPAdapter(max_retries=retries))

    s = requests.get(url)
    soup = BeautifulSoup(s.text, 'html.parser')
    links = []
    for link in soup.find_all('a'):
        links.append(link.get('href'))
    links.remove('adresses-france.csv.gz')
    links.remove('../')
    path = './data/gz/'
    if not os.path.exists(path):
        os.makedirs(path)

    for link in links:
        print(link)
        s = requests.get(url+link)
        with open(path+link, 'wb') as f:
            f.write(s.content) 

# -----
# MAIN
# -----
if __name__ == "__main__":
    print('Acquisition Script')
    query_bal()
    query_mes_adresses()
    query_ban()
    #query_decoupage_administratif()



