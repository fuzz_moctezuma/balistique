#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 28 05:08:45 2021

@author: cactus
"""

# ---------------
#     IMPORTS
# ---------------
import re
import pandas as pd

from scripts.completion import get_fresh_pickle, dump_pickle


def consolidation_densite_into_adm(densite, adm):
    def padding(x):
        if len(x)==4:
            return '0'+str(x)
        else:
            return x
    densite['Code Commune'] = densite['Code Commune'].apply(padding)
    densite.drop(['Libellé des communes', 'Région', 'Population municipale 2017'], axis=1, inplace=True)
    adm = adm.merge(densite,how='outer', left_on='code', right_on='Code Commune')
    adm.rename(columns={'Degré de Densité de la commune':'DENSITE'},inplace=True)
    adm.drop(columns=['Code Commune'], axis=1, inplace=True)
    return adm

def consolidation_epci_into_adm(epci, adm):
    """
    col name : codeEpci
    type : string
    descr : 

    col name : nomEpci
    type : string
    descr : 

    col name : typeEpci
    type : string
    values : CC CU CA ME CI
    descr : 

    """
    epci.drop(['LIBGEO', 'DEP', 'REG'], axis=1, inplace=True)
    adm = adm.merge(epci,how='outer', left_on='code', right_on='CODGEO')
    adm.drop('CODGEO', axis=1, inplace=True)
    adm.rename(columns={'EPCI':'codeEpci','LIBEPCI':'nomEpci'},inplace=True)
    adm['typeEpci'] = pd.Series(dtype=object)
    for r, row in adm.iterrows():
        if pd.isna(row.nomEpci)==False:
            if re.match("^['C']{2}",row.nomEpci):
                adm.loc[r, 'typeEpci']='CC'
            elif re.match("^['C']['U']",row.nomEpci):
                adm.loc[r, 'typeEpci']='CU'
            elif re.match("^['C']['A']",row.nomEpci):
                adm.loc[r, 'typeEpci']='CA'
            elif re.match(".*[mM]étropole.*",row.nomEpci):
                adm.loc[r, 'typeEpci']='ME'
            else :
                adm.loc[r, 'typeEpci']='CI'
    return adm

def consolidation_adr_into_adm(adr, adm):
    """

    """
    df = adm.merge(adr,how='outer', left_on='code', right_on='code')
    df.drop(columns=['nom_y'],axis=1,inplace=True)
    df.rename(columns = {
            'nom_x':'nom',
            '_id':'mesAdresses_id',
            '_id':'mesAdresses_id',
            'url':'mesAdresses_url',
            'email':'mesAdresses_email',
            '_created':'mesAdresses_created',
            '_updated':'mesAdresses_updated',
            },inplace=True)
    return df

def consolidation_bal_into_adm(bal, adm):
    df = adm.merge(bal,how='outer', left_on='code', right_on='commune_code')
    df.drop(columns=[
        'commune_code',
        'commune_nom',
        ],axis=1,inplace=True)
    df.rename(columns = {
        'editeur_license':'editeur_licence',
        'commune_type': 'bal_type',
        'commune_rowsCount': 'bal_rowsCount',
        'commune_dateMAJ': 'bal_dateMAJ',
        'commune_date_creation':'bal_date_creation'
        },inplace=True)        
    return df

def consolidated_frame():
    adm = pd.read_pickle(get_fresh_pickle('decoupage_administratif'))
    bal = pd.read_pickle(get_fresh_pickle('bal_datasets'))
    adr = pd.read_pickle(get_fresh_pickle('mes_adresses'))
    epci = pd.read_csv('./data/external_sources/liste_epci_insee_2021.csv')
    densite = pd.read_csv('./data/external_sources/grille_densite_2020_agrege.csv')
    adm = consolidation_densite_into_adm(densite, adm)
    adm = consolidation_epci_into_adm(epci, adm)
    adm = consolidation_adr_into_adm(adr,adm)
    df = consolidation_bal_into_adm(bal, adm)
    
    col_names = {
                'nom':'NOM',
                'nomRegion':'NOM_REGION',
                'nomDepartement':'NOM_DEPARTEMENT',
                'code':'CODE',
                'codeDepartement':'CODE_DEPARTEMENT',
                'codeRegion':'CODE_REGION',
                'codesPostaux':'CODES_POSTAUX',
                'population':'POPULATION',
                'contour':'CONTOUR',
                'contour_type':'CONTOUR_TYPE',
                'centre':'CENTRE',
                'surface':'SURFACE',
                'codeEpci':'EPCI_CODE',
                'nomEpci':'EPCI_NOM',
                'typeEpci':'EPCI_TYPE',
                'mesAdresses_id':'MESADRESSES_ID',
                'mesAdresses_url':'MESADRESSES_URL',
                'mesAdresses_created':'MESADRESSES_CREATED',
                'mesAdresses_updated':'MESADRESSES_UPDATED',
                'mesAdresses_email':'MESADRESSES_EMAIL',
                'bal_type':'BAL_TYPE',
                'bal_rowsCount':'BAL_ROWSCOUNT',
                'bal_dateMAJ':'BAL_DATEMAJ',
                'editeur_id':'EDITEUR_ID',
                'editeur_title':'EDITEUR_TITLE',
                'editeur_description':'EDITEUR_DESCRIPTION',
                'editeur_page':'EDITEUR_PAGE',
                'editeur_model':'EDITEUR_MODEL',
                'editeur_licence':'EDITEUR_LICENCE',
                'editeur_rowsCount':'EDITEUR_ROWSCOUNT',
                'editeur_errored':'EDITEUR_ERRORED',
                'editeur_url':'EDITEUR_URL',
                'editeur_dateMAJ':'EDITEUR_DATEMAJ',
                'editeur_isValid':'EDITEUR_ISVALID',
                'editeur_datagouv_nom':'EDITEUR_DATAGOUV_NOM',
                'editeur_datagouv_page':'EDITEUR_DATAGOUV_PAGE',
                'editeur_datagouv_logo':'EDITEUR_DATAGOUV_LOGO',
                'editeur_contour_type':'EDITEUR_CONTOUR_TYPE',
                'editeur_contour_coordinates':'EDITEUR_CONTOUR_COORDINATES',
                'bal_date_creation':'BAL_DATE_CREATION',
                'bal_date_creation':'BAL_DATE_CREATION'}
    df.rename(columns=col_names, inplace=True)    
    # CAN add df.reindex(columns=['']) to reaorder columns
    
    dump_pickle(df,'mainframe')
    return df


