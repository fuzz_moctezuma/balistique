#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 28 01:27:04 2021

@author: cactus
"""
# ---------------
#     IMPORTS
# ---------------
from scripts.acquisition import query_mes_adresses, query_bal, query_decoupage_administratif, query_ban
from scripts.completion import dump_pickle
from scripts.completion import parse_mes_adresses, parse_bal, parse_adm, parse_ban

# -----------------------------------
#              adr 
# -----------------------------------
def build_mes_adresses():
    """
    Write a PICKLE with newest Mes Adresses data.
        query_mes_adresses() > json_file
            load_json_from_file(json_file) > json_object
                read_simple_json(json_object) > dataframe
                    parse_mes_adresses(dataframe) > dataframe
    """
    filename = 'mes_adresses'
    # Acquisition
    query_mes_adresses()    
    # Completion
    df = parse_mes_adresses(filename)
    # Generation
    dump_pickle(df, filename)
# -----------------------------------
#              bal 
# -----------------------------------
def build_bal(deduplicate=True, as_file=True, as_frame=False):
    """
    Write a PICKLE with newest Base Adresse Locale data.
    query_bal() > json_file
        load_json_from_file(json_file) > json_object
            read_json_bal(json_object) > dataframe
                parse_bal(dataframe) > dataframe
                    recover_date_creation_bal(dataframe)>dataframe
                        manual_recover_date_creation_bal(dataframe)>dataframe
                            deduplicate_bal(dataframe) > dataframe
    """
    filename = 'bal'
    # Acquisition
    query_bal()
    # Completion
    bal = parse_bal(filename,deduplicate)
    # Generation
    if as_file==True:
        dump_pickle(bal, filename)
    if as_frame==True:
        return bal

# -----------------------------------
#              adm 
# -----------------------------------
def build_decoupage_administratif(geo_names=True, geo_data=True):
    """
    query_decoupage_administratif() > json_file
        load_json_from_file(json_file) > json_object
            read_simple_json(json_object) > dataframe
                query_geo_data_communes(dataframe) > dataframe
                    dump_pickle(dataframe) > pickle
    """
    filename = 'decoupage_administratif'
    
    # Acquisition
    query_decoupage_administratif()    
    
    # Completion
    df = parse_adm(filename,geo_names, geo_data)
    # Generation
    dump_pickle(df, filename)

def build_ban():
    """
    query_ban()> csv.gz files
        parse_ban(csv.gz.file(csv.gz)>dataframe)
            dump_pickle(dataframe)>pickle
    """
    filename='ban'
    # Acquisition
    query_ban()
    # Completion
    df = parse_ban(filename='adresses')
    # Generation
    dump_pickle(df,filename)


if __name__ == "__main__":
    print('Generation Script')
     
     #build_decoupage_administratif(geo_data=True, geo_names=True)
    
    
