#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 28 01:25:19 2021

@author: cactus
"""

# ---------------
#     IMPORTS
# ---------------
import os
import time
from datetime import datetime
import json
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import logging
import pandas as pd
import gzip

from scripts.acquisition import request_json_data


# -----------------------------------------------------------------------
# FONCTIONS : Traitement des JSON en local pour compléter les DataFrames
# -----------------------------------------------------------------------
def load_json_from_file(filename):
    """
    Given a JSON file name from /data/json/, return it into a JSON object
    """
    path = './data/json/'
    f = open(path+filename)
    js = json.load(f)
    return js

def read_simple_json(js):
    """
    Load a JSON object into a DataFrame
    """
    df = pd.json_normalize(js)    
    return df    

def dump_pickle(df,df_name):
    """
    Write a DataFrame to a PICKLE file in folder /data/pickle/ with the date
    """
    # Get todays date
    dt_string = datetime.now().strftime("%Y_%m_%d_%H%M")
    # Create folder if none existant
    path = './data/pickle/'
    if not os.path.exists(path):
        os.makedirs(path)
    #Write the dataframe to pickle file 
    df.to_pickle(path+df_name+'_'+dt_string+'.pkl')

def get_fresh_pickle(filename):
    """
    Return name of latest pickle file
    """
    path = './data/pickle/'
    files = []
    for file in os.listdir(path):
        if filename in file:
            files.append(file)
    files.sort(reverse=True)
    print(files[0])
    return path+files[0]    

def get_fresh_json(filename):
    """
    Return name of latest json file without folder path
    """
    path='./data/json/'
    files = []
    for file in os.listdir(path):
        if filename in file:
            files.append(file)
    files.sort(reverse=True)
    return files[0]
    
# -------------------------
#            adr
# -------------------------
def parse_mes_adresses(filename):
    """
    Load raw Mes Adresses DataFrame and return it with correct dtypes and columns names.
    """
    df = read_simple_json(load_json_from_file(filename=get_fresh_json(filename)))
    df['_updated'] = pd.to_datetime(df['_updated'],infer_datetime_format=True)
    df['_created'] = pd.to_datetime(df['_created'],infer_datetime_format=True)
    df.rename(columns={'commune.code':'code','commune.nom':'nom','commune.email':'email'}, inplace=True)
    print("Parsing completed.")
    return df

# -----------------------
#          bal 
# -----------------------
def read_json_bal(js):
    """
    Given a JSON file from API endpoints: backend.adresse.gouv.fr/datasets,
    return flattened DataFrame with each row a commune.
    """
    # Load JSON into DataFrame object and flatten nested content
    meta_cols = ['id', 'title', 'description', 'page', 'model', 'license', 'rowsCount','errored',  'url','dateMAJ', 'isValid']
    df0 = pd.json_normalize(js, record_path=['communes'], record_prefix='commune_' , meta=meta_cols, meta_prefix='editeur_',  errors='ignore')
    df1 = pd.json_normalize(js)[['id','organization.name','organization.page','organization.logo','contour.type','contour.coordinates']]
    df = pd.merge(df0,df1,left_on=['editeur_id'],right_on=['id'])
    # 'id' is duplicate from 'editeur_id'
    df.drop('id', inplace=True, axis=1)
    return df


def fix_columns_bal(df):
    """        
    Load raw Bal Datasets DataFrame and return it with correct dtypes and columns names.
    """
    # making old datasets (<2021) and new ones interoperable. Old ones dont have 
    # commune_nom, commune_type, commune_rowsCount, commune_dateMAJ but only commune_0
    if 'commune_0' in df.columns:
        df.rename(columns={'commune_0':'commune_code'}, inplace=True)
        df['commune_dateMAJ'] = df['editeur_dateMAJ']
        df['commune_type'] = pd.Series(dtype=str)
        df['commune_rowsCount'] = pd.Series(dtype=float)
    # Renaming columns
    df.rename(columns={'organization.name':'editeur_datagouv_nom',
                       'organization.page':'editeur_datagouv_page',
                       'organization.logo':'editeur_datagouv_logo',
                       'contour.type':'editeur_contour_type',
                       'contour.coordinates':'editeur_contour_coordinates'},
              inplace=True)
    
    df['commune_dateMAJ'] = pd.to_datetime(df['commune_dateMAJ'],format='%Y-%m-%d')#infer_datetime_format=True)
    df['editeur_dateMAJ'] = pd.to_datetime(df['editeur_dateMAJ'],format='%Y-%m-%d')#infer_datetime_format=True)
    
    return df

def recover_date_creation_bal(df):
    """
    From the collection of bal JSON files (included histo-datasets.zip)
    Recover most ancient dateMAJ to assign as creation date
    """
    print('Recovering creation dates from archives... ', end='')
    list_json_datasets = []
    for json_file in os.listdir('./data/json'):
        if 'bal_' in json_file:
            list_json_datasets.append(json_file)
    list_json_datasets.sort(reverse=True)
    
    # Creating date_creation column
    df['commune_date_creation'] = pd.Series(dtype=object)
    # Converting to datetime objects relevant columns
    df['commune_date_creation'] = pd.to_datetime(df['commune_date_creation'], infer_datetime_format=True)
    
    for i in list_json_datasets:
        old = fix_columns_bal(read_json_bal(load_json_from_file(filename=i))) 
        old.dropna(how='all',subset=['commune_dateMAJ','editeur_dateMAJ'], inplace=True)
        old = old.sort_values('commune_dateMAJ').drop_duplicates('commune_code',keep='first')
        
        for row in df.loc[ df.commune_code.isin(old.commune_code)].itertuples():
            old_row = old.loc[ old.commune_code==row.commune_code ].iloc[0]
            earlier_date = pd.Series([row.commune_dateMAJ, old_row.commune_dateMAJ, row.editeur_dateMAJ, old_row.editeur_dateMAJ]).min()
            df.loc[row[0], 'commune_date_creation'] = earlier_date
    print('Dates recovered')
    return df
    
def manual_recover_date_creation_bal(df):
    print('Recovering creation dates from manual listing... ', end='')
    dates_manuelles = pd.read_csv('./data/external_sources/liste_date_creation_manquantes.csv')
    dates_manuelles['date_iso'] = pd.to_datetime(dates_manuelles['date_iso'],format='%Y-%m-%d')
    for r, row in dates_manuelles.iterrows():
        df.loc[ df.editeur_title == row.editeur ,  'commune_date_creation' ] = row.date_iso    
    # IS A BAD IDEA BUT SIMPLE FIX TO MISSING DATES
    # Dates : filling missing dates with most ancient
    df.loc[ df[df.commune_date_creation.isna()].index , 'commune_date_creation'] = df.commune_date_creation.min()
    # Fixing impossible outlier TEMPORARY /!\ # Fixing impossible outlier TEMPORARY /!\# Fixing impossible outlier TEMPORARY /!\
    df.loc[df.commune_nom=='Revest-Saint-Martin','commune_date_creation'] = pd.Timestamp('20210308') # Fixing impossible outlier TEMPORARY /!\
    print('Dates recovered')
    return df


def deduplicate_bal(df):
    # Dropping duplicates and keeping most recent ones
    # /!\ existing duplicates can be a valuable information to see how switched publishing tools!
    df = df.sort_values('commune_date_creation').drop_duplicates('commune_nom',keep='last')
    return df

def parse_bal(filename,deduplicate):
    df = fix_columns_bal(read_json_bal( load_json_from_file(filename=get_fresh_json(filename) )))
    df = recover_date_creation_bal(df)
    df = manual_recover_date_creation_bal(df)
    if deduplicate==True :
        df = deduplicate_bal(df)
    
    print("Parsing completed.")
    return df


# -----------------------------------
#               adm
# -----------------------------------
def query_geo_data_communes(df):
    """
    Iterate adm DataFrame and fetch additional data from geo.api.gouv.fr :
     - surface: float, en m2
     - contour: list polygon coordonnées lat lon 
     - center:  list coordonnées lat lon
    /!\ Takes aprox. 35000 seconds to run because of API query limitations.
    """
    logging.basicConfig(level=logging.DEBUG, filename='./logs/lumberjack.log')          
    s = requests.Session()
    retries = Retry(total=100, backoff_factor=1, status_forcelist=[ 502, 503, 504 ])
    s.mount('https://', HTTPAdapter(max_retries=retries))
    
    df['contour' ] = pd.Series(dtype =object)
    df['centre'] = pd.Series(dtype =object)
    
    for i, row in df.iterrows():
        try:
            code_commune_INSEE = df.at[i,'code']
            r = s.get('https://geo.api.gouv.fr/communes?code={0}&fields=code,nom,surface,centre,contour'.format(code_commune_INSEE))
            print(r.status_code)
            r = r.json()                         
            df.at[ i , 'surface' ] = r[0]['surface']
            df.at[ i , 'contour_type' ] = r[0]['contour']['type']
            df.at[ i , 'contour' ] = r[0]['contour']['coordinates']
            df.at[i,'centre' ] = r[0]['centre']['coordinates']
        except:
            print('ERROR')
            print(df.at[i,'code'])
    return df


def query_geo_names(df):
    r = requests.get('https://geo.api.gouv.fr/regions')
    for region in r.json():
        df.loc[ df.codeRegion==region['code'] ,'nomRegion'] = region['nom']
    for code_region in df.codeRegion.value_counts().index:
        r = requests.get('https://geo.api.gouv.fr/regions/{}/departements'.format(str(code_region)))
        for departement in r.json():
            df.loc[ df.codeDepartement==departement['code'] ,'nomDepartement'] = departement['nom']
    return df

def parse_adm(filname, geo_names, geo_data):
    filename = 'decoupage_administratif'
    # Acquisition
    df = read_simple_json(load_json_from_file(filename=get_fresh_json(filename)))
    # Completion
    if geo_names==True:
        df= query_geo_names(df)
    if geo_data==True:
        df = query_geo_data_communes(df)
    return df

#-----------------------
#         ban
#-----------------------
def parse_ban(filename):
    """It is very memory intensive"""
    path = './data/gz/'
    files = []
    for file in os.listdir(path):
        if filename in file:
            files.append(file)
    files.sort(reverse=False)
    with gzip.open(path+files[0]) as f:
        df = pd.read_csv(f, delimiter=';', low_memory=False)
    for file in files[1:]:
        if 'adresses' in file:
            print(file)
            with gzip.open(path+file) as f:
                df = df.append(pd.read_csv(f, delimiter=';', low_memory=False))
    return df









if __name__ == "__main__":
    print('Completion Script')
    












